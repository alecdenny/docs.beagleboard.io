.. _beagley-ai-quick-start:

BeagleY-AI Quick Start
######################

.. important:: Coming Soon!


For now, see https://www.beagleboard.org/app/uploads/2024/03/Getting-started-with-your-BeagleY-AI.pdf
